# clima_piura

## Requisitos

1. Comando curl
2. Comando cat
3. Internet

## Primeros pasos

Creamos la carpeta oculta en el /home/${USER}/.clima

$ mkdir ~/.clima

Copiamos el archivo a $PATH (Puedes saber la ubicación con echo $PATH)

$ sudo cp clima /usr/local/bin/

## Uso

Ejecutamos escribiendo la palabra clima, muestra el clima de hoy y se almacena el clima para mañana.

$ clima

## Recomendaciones

Si vive en otro lugar del mundo puede cambiar la ubicación Piura por la ciudad que reside en el mismo programa de clima.
